# Wikipedia Table to CSV

This project aim to extract Wikipedia tables into CSV files.
Test can be launched with the following commands after cloning :

```
cd wikimatrix
mvn test
```

CSV files can be found in the output/html folder.

# Architecture

![](classDiagram.png)

# Copyright

This project is a fork from : [https://github.com/acherm/wikipediamatrix-bench](https://github.com/acherm/wikipediamatrix-bench)

This project is under CC-BY-NC (copy, distribution, display, of the original or modified project for non-commercial purposes is allowed upon giving credits to the author)
