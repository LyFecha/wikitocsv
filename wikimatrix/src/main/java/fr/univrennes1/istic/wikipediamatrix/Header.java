package fr.univrennes1.istic.wikipediamatrix;

import java.util.ArrayList;
import java.util.LinkedHashSet;

public class Header extends CSVPart {

    private final String HEADER_SEPARATION = " / ";

    @Override
    public String render(String delimiter, String quote) {
        if (getContent().size() == 0)
            return "";
        StringBuffer buffer = new StringBuffer();
        ArrayList<ArrayList<String>> content = new ArrayList<ArrayList<String>>(
                new LinkedHashSet<ArrayList<String>>(getContent())); // removing duplicate header
        for (int colInd = 0; colInd < getLongestListLength(content); colInd++) {
            String lastHeaderName = "";
            StringBuffer col = new StringBuffer();
            for (ArrayList<String> line : content) {
                if (colInd < line.size() && !line.get(colInd).equals(lastHeaderName)) {
                    col.append(HEADER_SEPARATION).append(line.get(colInd));
                    lastHeaderName = line.get(colInd);
                }
            }
            if (col.length() > 0) {
                col.delete(0, HEADER_SEPARATION.length());
            }
            buffer.append(quote);
            buffer.append(col.toString().replaceAll(quote, "\"" + quote)); // escaping quote
            buffer.append(quote + delimiter);
        }
        if (buffer.length() > 0) {
            buffer.deleteCharAt(buffer.length() - 1);
        }
        buffer.append("\n");
        return buffer.toString();
    };

    private int getLongestListLength(ArrayList<ArrayList<String>> list) {
        int longestListSize = list.get(0).size();
        for (ArrayList<String> subList : list) {
            if (subList.size() > longestListSize) {
                longestListSize = subList.size();
            }
        }
        return longestListSize;
    }
}
