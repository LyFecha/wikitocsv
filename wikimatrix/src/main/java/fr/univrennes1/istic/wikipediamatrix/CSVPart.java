package fr.univrennes1.istic.wikipediamatrix;

import java.util.ArrayList;

import org.jsoup.nodes.Element;

public abstract class CSVPart {

    private ArrayList<ArrayList<String>> content;
    private int linePointer = 0;

    public CSVPart() {
        this.content = new ArrayList<ArrayList<String>>();
    }

    public int addLine(Element line) {
        int colPointer = 0;
        int nbMaxLinesToAdd = 0;
        for (Element col : line.children()) {
            int nbColToAdd = col.hasAttr("colspan") ? Integer.parseInt(col.attr("colspan")) : 1;
            int nbLineToAdd = col.hasAttr("rowspan") ? Integer.parseInt(col.attr("rowspan")) : 1;
            nbMaxLinesToAdd = Math.max(nbMaxLinesToAdd, nbLineToAdd - 1);
            for (int colSubInd = 0; colSubInd < nbColToAdd; colSubInd++) {
                for (int lineSubInd = 0; lineSubInd < nbLineToAdd; lineSubInd++) {
                    int colInd = colPointer + colSubInd;
                    int lineInd = linePointer + lineSubInd;
                    addNullIntoContent(lineInd, colInd);
                    setIntoContent(lineInd, colInd, col.text());
                }
            }
            colPointer += nbColToAdd;
        }
        linePointer += 1;
        return nbMaxLinesToAdd;
    }

    public abstract String render(String delimiter, String quote);

    protected void addNullIntoContent(int lineInd, int colInd) {
        // Those two loops only trigger when lineInd and/or colInd are out of bounds
        for (int i = 0; i <= lineInd - content.size(); i++) {
            content.add(new ArrayList<String>());
        }
        for (int j = 0; j < colInd - content.get(lineInd).size(); j++) {
            content.get(lineInd).add(null);
        }
    }

    protected void setIntoContent(int lineInd, int colInd, String value) {
        ArrayList<String> contentLine = getContent().get(lineInd);
        if (colInd < contentLine.size()) {
            int indexToAdd = contentLine.indexOf(null);
            if (indexToAdd >= 0) {
                contentLine.set(indexToAdd, value);
            } else {
                contentLine.add(value);
            }
        } else {
            contentLine.add(value);
        }
    }

    protected ArrayList<ArrayList<String>> getContent() {
        return content;
    }
}
