package fr.univrennes1.istic.wikipediamatrix;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class CSVSaver {

    public void saveToCSV(String csv, String name, int number) {
        // System.out.println(csv);
        String filename = name + "-" + Integer.toString(number) + ".csv";
        try {
            writeToPath("wikimatrix\\output\\html\\" + filename, csv);
        } catch (IOException ex) {
            try {
                writeToPath("output\\html\\" + filename, csv);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void saveToCSV(String[] csvs, String name) {
        for (int i = 0; i < csvs.length; i++) {
            saveToCSV(csvs[i], name, i);
        }
    }

    private void writeToPath(String pathname, String content) throws IOException {
        File csvFile = new File(pathname);
        if (csvFile.exists()) {
            csvFile.delete();
        }
        csvFile.createNewFile();
        FileWriter myWriter = new FileWriter(pathname);
        myWriter.write(content);
        myWriter.close();
    }

}