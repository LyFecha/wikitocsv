package fr.univrennes1.istic.wikipediamatrix;

import java.util.ArrayList;

public class Body extends CSVPart {

    @Override
    public String render(String delimiter, String quote) {
        StringBuffer buffer = new StringBuffer();
        for (ArrayList<String> line : getContent()) {
            for (String col : line) {
                buffer.append(quote);
                buffer.append(col.replaceAll(quote, "\"" + quote)); // escaping quote
                buffer.append(quote + delimiter);
            }
            if (buffer.length() > 0) {
                buffer.deleteCharAt(buffer.length() - 1);
            }
            buffer.append("\n");
        }
        return buffer.toString();
    };

}
