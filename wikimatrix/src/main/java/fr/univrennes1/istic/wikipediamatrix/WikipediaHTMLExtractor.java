package fr.univrennes1.istic.wikipediamatrix;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class WikipediaHTMLExtractor {

  private static final Logger logger = LogManager.getLogger("WikipediaHTMLExtractor");

  public static void main(String[] args) {
    try {
      String[] csvs = new WikipediaHTMLExtractor().extract(";", "\"",
          "https://en.wikipedia.org/wiki/Comparison_between_Esperanto_and_Ido");
      (new CSVSaver()).saveToCSV(csvs, "_test");
      logger.info(csvs[0]);
    } catch (IOException e) {
      logger.info("La connexion a échoué, le site est probablement down (404)");
    }
  }

  public String[] extract(String delimiter, String quote, String pageName) throws IOException {
    Document doc;
    ArrayList<String> csvs = new ArrayList<String>();
    doc = Jsoup.connect(pageName).get();
    // logger.info(doc.title());
    Elements tables = doc.getElementById("mw-content-text").getElementsByClass("wikitable");
    for (Element table : tables) {
      // If it's a navigation table (thing at the end of a wikipage), we don't take
      // into consideration
      if (table.hasClass("metadata") || table.parents().hasClass("navbox"))
        continue;
      Body body = new Body();
      Header header = new Header();
      CSVPart currentPart = header;
      int nbFollowingLineToAdd = 0;
      for (Element line : table.getElementsByTag("tbody").get(0).select("> tr")) {
        int nbMaxLinesToAdd = 0;
        if (nbFollowingLineToAdd > 0) {
          nbFollowingLineToAdd -= 1;
        } else if (line.getElementsByTag("td").size() > 0) {
          currentPart = body;
        } else if (line.getElementsByTag("th").size() == 1) {
          // We remove headers that takes a whole line because they doesn't add any
          // information and takes a lot of space in files and crash sometimes.
          continue;
        } else {
          currentPart = header;
        }
        nbMaxLinesToAdd = currentPart.addLine(line);
        nbFollowingLineToAdd = Math.max(nbMaxLinesToAdd, nbFollowingLineToAdd);
      }
      String csv = header.render(delimiter, quote) + body.render(delimiter, quote);
      csvs.add(csv);
      // logger.info(csv);
    }
    return csvs.toArray(new String[0]);
  }
}
