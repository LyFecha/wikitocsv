package fr.univrennes1.istic.wikipediamatrix;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.math3.stat.descriptive.SummaryStatistics;

public class Stats {
    private ArrayList<Integer> cellCount;
    private ArrayList<Integer> colCount;
    private ArrayList<Integer> lineCount;

    public Stats() {
        this.cellCount = new ArrayList<Integer>();
        this.colCount = new ArrayList<Integer>();
        this.lineCount = new ArrayList<Integer>();
    }

    public void addToStats(String[] csvs, char delimiter, char quote) {
        for (String csv : csvs) {
            CSVFormat format = CSVFormat.DEFAULT.withDelimiter(delimiter).withQuote(quote);
            CSVParser csvParsed;
            try {
                int csvCellCount = 0;
                int csvLineCount = 0;
                int maxColCount = 0;
                csvParsed = CSVParser.parse(csv, format);
                for (CSVRecord record : csvParsed.getRecords()) {
                    csvLineCount++;
                    maxColCount = Math.max(maxColCount, record.size());
                    csvCellCount += record.size();
                }
                this.cellCount.add(csvCellCount);
                this.colCount.add(maxColCount);
                this.lineCount.add(csvLineCount);
            } catch (IOException e) {
                return;
            }
        }
    }

    public String render() {
        SummaryStatistics statLine = new SummaryStatistics();
        SummaryStatistics statCol = new SummaryStatistics();
        SummaryStatistics statCell = new SummaryStatistics();
        for (Integer line : lineCount) {
            statLine.addValue(line);
        }
        for (Integer col : colCount) {
            statCol.addValue(col);
        }
        for (Integer cell : cellCount) {
            statCell.addValue(cell);
        }
        StringBuffer stats = new StringBuffer();
        stats.append("Varibale | Number | Min | Max | Mean | Std\n");
        calculateStats("Lines", stats, statLine);
        calculateStats("Columns", stats, statCol);
        calculateStats("Cells", stats, statCell);
        return stats.toString();
    }

    private void calculateStats(String name, StringBuffer display, SummaryStatistics stats) {
        display.append(name).append(" | ").append(stats.getSum()).append(" | ").append(stats.getMin()).append(" | ")
                .append(stats.getMax()).append(" | ").append(stats.getMean()).append(" | ")
                .append(stats.getStandardDeviation()).append("\n");
    }
}
