package fr.univrennes1.istic.wikipediamatrix;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.junit.Test;

public class WikipediaHTMLExtractorTest {

  String BASE_WIKIPEDIA_URL = "https://en.wikipedia.org/wiki/";

  private CSVParser parseCSV(String[] csvs) throws IOException {
    CSVFormat format = CSVFormat.DEFAULT.withDelimiter(';').withQuote('\"');
    CSVParser csvParsed = CSVParser.parse(csvs[0], format);
    return csvParsed;
  }

  @Test
  public void shouldExtractReturnCSV() throws IOException {
    String[] csvs = new WikipediaHTMLExtractor().extract(";", "\"", BASE_WIKIPEDIA_URL + "Comparison_of_digital_SLRs");
    try {
      CSVParser csvParsed = parseCSV(csvs);
      List<CSVRecord> records = csvParsed.getRecords();
      assertTrue(Integer.toString(records.size()), records.size() == 72);
      for (CSVRecord record : records) {
        assertTrue(Integer.toString(record.size()), record.size() == 22);
      }
    } catch (IOException e) {
      e.printStackTrace();
      assertTrue("Le parsage ne s'est pas bien passé", false);
    }
  }

  @Test
  public void shouldExtractFailWithCommaAndFormat() throws IOException {
    String[] csvs = new WikipediaHTMLExtractor().extract(",", "\"", BASE_WIKIPEDIA_URL + "Comparison_of_digital_SLRs");
    try {
      CSVParser csvParsed = parseCSV(csvs);
      List<CSVRecord> records = csvParsed.getRecords();
      assertTrue(Integer.toString(records.size()), records.size() == 72);
      for (CSVRecord record : records) {
        assertFalse(Integer.toString(record.size()), record.size() == 22);
      }
    } catch (IOException e) {
      assertTrue(true);
    }
  }

  @Test
  public void shouldExtractFailWithOtherPage() throws IOException {
    String[] csvs = new WikipediaHTMLExtractor().extract(";", "\"",
        BASE_WIKIPEDIA_URL + "Comparison_between_Esperanto_and_Ido");
    try {
      CSVParser csvParsed = parseCSV(csvs);
      List<CSVRecord> records = csvParsed.getRecords();
      assertFalse(Integer.toString(records.size()), records.size() == 72);
      for (CSVRecord record : records) {
        assertFalse(Integer.toString(record.size()), record.size() == 22);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
