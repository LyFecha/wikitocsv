package fr.univrennes1.istic.wikipediamatrix;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.jsoup.HttpStatusException;
import org.junit.Test;

public class BenchTest {

	/*
	 * the challenge is to extract as many relevant tables as possible and save them
	 * into CSV files from the 300+ Wikipedia URLs given see below for more details
	 **/
	@Test
	public void testBenchExtractors() throws Exception {

		String BASE_WIKIPEDIA_URL = "https://en.wikipedia.org/wiki/";
		// directory where CSV files are exported (HTML extractor)
		String outputDirHtml = "output" + File.separator + "html" + File.separator;
		assertTrue(new File(outputDirHtml).isDirectory());
		// directory where CSV files are exported (Wikitext extractor)
		String outputDirWikitext = "output" + File.separator + "wikitext" + File.separator;
		assertTrue(new File(outputDirWikitext).isDirectory());

		File file = new File("inputdata" + File.separator + "wikiurls.txt");
		BufferedReader br = new BufferedReader(new FileReader(file));
		String url;
		int nurl = 0;
		// Added lines by Maxence Gazeau :
		CSVSaver csvSaver = new CSVSaver();
		WikipediaHTMLExtractor extractor = new WikipediaHTMLExtractor();
		int pageNotFoundCounter = 0;
		int tableSavedCounter = 0;
		int pageSavedCounter = 0;
		int extractionPageErrorCounter = 0;
		ArrayList<String> malformedCsvs = new ArrayList<String>();
		Stats stats = new Stats();
		while ((url = br.readLine()) != null) {
			String wurl = BASE_WIKIPEDIA_URL + url;
			System.out.println("Wikipedia url: " + wurl);
			// (ie extract relevant tables for correct URL, with the two extractors)

			// for exporting to CSV files, we will use mkCSVFileName
			// example: for
			// https://en.wikipedia.org/wiki/Comparison_of_operating_system_kernels
			// the *first* extracted table will be exported to a CSV file called
			// "Comparison_of_operating_system_kernels-1.csv"
			// String csvFileName = mkCSVFileName(url, 1);
			// System.out.println("CSV file name: " + csvFileName);
			// the *second* (if any) will be exported to a CSV file called
			// "Comparison_of_operating_system_kernels-2.csv"
			try {
				String[] csvs = extractor.extract(";", "\"", wurl);
				stats.addToStats(csvs, ';', '\"');
				csvSaver.saveToCSV(csvs, url);
				malformedCsvs.addAll(checkSizeCsvs(csvs, url));
				tableSavedCounter += csvs.length;
				pageSavedCounter++;
			} catch (HttpStatusException ex) {
				System.out.println("----> Erreur d'accès à la page, elle n'existe probablement plus (404)");
				pageNotFoundCounter++;
			} catch (Exception ex) {
				System.out.println("----> L'extraction a rencontré une erreur :");
				ex.printStackTrace();
				extractionPageErrorCounter++;
			}

			nurl++;
			// if (nurl == 10) {
			// break;
			// }
		}
		System.out.println("Number of page saved : " + Integer.toString(pageSavedCounter));
		System.out.println("Number of csv saved : " + Integer.toString(tableSavedCounter));
		System.out.println("Number of 4XX status exceptions : " + Integer.toString(pageNotFoundCounter));
		System.out.println("Number of page extraction failures : " + Integer.toString(extractionPageErrorCounter));
		System.out.println("Number of malformed csvs : " + Integer.toString(malformedCsvs.size()));
		System.out.println(stats.render());
		System.out.println("Malformed csvs : ");
		for (String name : malformedCsvs) {
			System.out.println(name);
		}

		br.close();
		assertEquals(nurl, 336);

	}

	private ArrayList<String> checkSizeCsvs(String[] csvs, String name) {
		ArrayList<String> malformedCsvs = new ArrayList<String>();
		for (int i = 0; i < csvs.length; i++) {
			String csv = csvs[i];
			if (csv.length() == 0) {
				malformedCsvs.add(name + "-" + i);
				continue;
			}
			CSVFormat format = CSVFormat.DEFAULT.withDelimiter(';').withQuote('\"');
			CSVParser csvParsed;
			try {
				csvParsed = CSVParser.parse(csv, format);
				int colCount = 0;
				boolean isMalformed = false;
				for (CSVRecord record : csvParsed.getRecords()) {
					if (colCount == 0) {
						colCount = record.size();
					} else if (record.size() > 0 && record.size() != colCount) {
						isMalformed = true;
						break;
					}
				}
				if (isMalformed) {
					malformedCsvs.add(name + "-" + i);
				}
			} catch (IOException e) {
				malformedCsvs.add(name + "-" + i);
			}
		}
		return malformedCsvs;
	}

	/*
	 * private String mkCSVFileName(String url, int n) { return url.trim() + "-" + n
	 * + ".csv"; }
	 */

}
